﻿using COVID19.COMMON.Entidades;
using COVID19.COMMON.Interfaces;
using System.Collections.Generic;

namespace COVID19.BIZ.API
{
    public class GenericManager : IGenericManager
    {
        RestService _restService;
        public GenericManager()
        {
            _restService = new RestService();
        }

        public Summary ListarPaises() => _restService.ListarPaises().Result;

        public List<History> TraerHistorialDeUnPais(string nombrePais) => _restService.TraerHistorialDeUnPais(nombrePais).Result;
    }
}
