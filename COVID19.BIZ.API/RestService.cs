﻿using COVID19.COMMON.Entidades;
using COVID19.COMMON.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace COVID19.BIZ.API
{
    public class RestService
    {
        private HttpClient _cliente;
        private string _uriApi;
        public string Error { get; set; }
        public RestService()
        {
            _cliente = new HttpClient { BaseAddress = new Uri("https://api.covid19api.com/") };
            _cliente.DefaultRequestHeaders.Clear();
            _cliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("appllication/json"));
            _cliente.MaxResponseContentBufferSize = 256000;
        }

        public async Task<Summary> ListarPaises()
        {
            HttpResponseMessage response = await _cliente.GetAsync("summary").ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                Summary items = JsonConvert.DeserializeObject<Summary>(content);
                return items;
            }
            else
            {
                return null;
            }
        }

        public async Task<List<History>> TraerHistorialDeUnPais(string nombrePais)
        {
            HttpResponseMessage response = await _cliente.GetAsync($"total/dayone/country/{nombrePais}").ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                List<History> items = JsonConvert.DeserializeObject<List<History>>(content);
                return items;
            }
            else
            {
                return null;
            }
        }
    }
}