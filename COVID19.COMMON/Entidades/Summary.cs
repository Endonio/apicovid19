﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace COVID19.COMMON.Entidades
{
    public class Summary
    {
        [JsonProperty("Global")]
        public Global Global { get; set; }

        [JsonProperty("Countries")]
        public Country[] Countries { get; set; }

        [JsonProperty("Date")]
        public DateTimeOffset Date { get; set; }
    }
}
