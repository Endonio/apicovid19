﻿using COVID19.COMMON.Entidades;
using System.Collections.Generic;

namespace COVID19.COMMON.Interfaces
{
    public interface IGenericManager
    {
        Summary ListarPaises();
        List<History> TraerHistorialDeUnPais(string nombrePais);
    }
}
