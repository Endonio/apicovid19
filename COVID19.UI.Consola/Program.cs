﻿using COVID19.BIZ.API;
using COVID19.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace COVID19.UI.Consola
{
    class Program
    {
        static void Main(string[] args)
        {
            GenericManager genericManager = new GenericManager();
            Summary paises = genericManager.ListarPaises();
            foreach (Country pais in paises.Countries.OrderByDescending(country=>country.TotalDeaths))
            {
                Console.WriteLine($"Pais: {pais.CountryCountry}. Casos Confirmados: {pais.TotalConfirmed}. Total de Muertes: {pais.TotalDeaths}");
            }

            List<History> histories = genericManager.TraerHistorialDeUnPais("united states of america");
            foreach (History history in histories.OrderBy(histor=>histor.Date))
            {
                Console.WriteLine($"Fecha y Hora: {history.Date}. Número de casos: {history.Confirmed}");
            }
            Console.ReadLine();
        }
    }
}
