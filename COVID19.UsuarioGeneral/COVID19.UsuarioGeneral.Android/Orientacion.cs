﻿using System;

using Android.App;
using Android.Content.PM;
using COVID19.UsuarioGeneral.Tools;
using Xamarin.Forms;

[assembly: Dependency(typeof(COVID19.UsuarioGeneral.Droid.Orientacion))]
namespace COVID19.UsuarioGeneral.Droid
{
    public class Orientacion:IOrientacion
    {
        [Obsolete]
        public void CambiarOrientacion(int opcion = 0)
        {
            if (opcion == 0)
            {
                ((Activity)Forms.Context).RequestedOrientation = ScreenOrientation.Portrait;
            }
            else
            {
                ((Activity)Forms.Context).RequestedOrientation = ScreenOrientation.Landscape;
            }
        }
    }
}