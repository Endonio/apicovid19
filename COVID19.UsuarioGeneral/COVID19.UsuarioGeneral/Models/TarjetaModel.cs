﻿namespace COVID19.UsuarioGeneral.Models
{
    public class TarjetaModel
    {
        public string Titulo { get; set; }
        public string NumeroMuerte { get; set; }
        public string NumeroConfirmados { get; set; }
    }
}
