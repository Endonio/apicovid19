﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COVID19.UsuarioGeneral.Tools
{
    public static class Herramientas
    {
        public static string RGBToHexadecimal(RGB rgb)
        {
            string rs = DecimalToHexadecimal(rgb.R);
            string gs = DecimalToHexadecimal(rgb.G);
            string bs = DecimalToHexadecimal(rgb.B);

            return '#' + rs + gs + bs;
        }

        private static string DecimalToHexadecimal(int dec)
        {
            if (dec <= 0)
                return "00";

            int hex = dec;
            string hexStr = string.Empty;

            while (dec > 0)
            {
                hex = dec % 16;

                if (hex < 10)
                    hexStr = hexStr.Insert(0, Convert.ToChar(hex + 48).ToString());
                else
                    hexStr = hexStr.Insert(0, Convert.ToChar(hex + 55).ToString());

                dec /= 16;
            }
            if (hexStr.Length != 2)
            {
                return $"0{hexStr}";
            }
            return hexStr;
        }
    }
}
