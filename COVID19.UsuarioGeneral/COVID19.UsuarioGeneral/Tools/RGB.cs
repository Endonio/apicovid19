﻿namespace COVID19.UsuarioGeneral.Tools
{
    public struct RGB
    {
        public RGB(byte r, byte g, byte b)
        {
            this.R = r;
            this.G = g;
            this.B = b;
        }

        public byte R { get; set; }

        public byte G { get; set; }

        public byte B { get; set; }

        public bool Equals(RGB rgb)
        {
            return (this.R == rgb.R) && (this.G == rgb.G) && (this.B == rgb.B);
        }
    }
}