﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace COVID19.UsuarioGeneral.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        private bool _isBusy = false;

        /// <summary>
        /// IsBusy: Verifica que las peticiones continuas, por parte del usuario, sean tomadas como una sola y no se repitan.
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        /// <summary>
        /// Metodo que dicta el cambio de propiedades en caso de ser necesario
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="backingStore">Tipo de las propiedades, en este caso boleano</param>
        /// <param name="value">Valor de las propiedades</param>
        /// <param name="propertyName">El nombre asignado con la llamada del string</param>
        /// <param name="onCahnged">Método de cambio, analiza la situacion actual de la propiedad</param>
        /// <returns></returns>
        protected bool SetProperty<T>(ref T backingStore, T value, [CallerMemberName] string propertyName = "", Action onCahnged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;
            backingStore = value;
            onCahnged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }
        /// <summary>
        /// Propiedad a cambiar por el evento
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Método que dicta el cambio de nombre desde la llamada 
        /// </summary>
        /// <param name="propertyName"></param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler changed = PropertyChanged;
            if (changed == null)
                return;
            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
