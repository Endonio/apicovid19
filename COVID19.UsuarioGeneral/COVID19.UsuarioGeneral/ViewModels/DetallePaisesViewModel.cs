﻿using COVID19.BIZ.API;
using COVID19.COMMON.Entidades;
using COVID19.UsuarioGeneral.Tools;
using Microcharts;
using Microcharts.Forms;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Xamarin.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace COVID19.UsuarioGeneral.ViewModels
{
    public class DetallePaisesViewModel:BaseViewModel
    {
        #region Propiedades
        private StackLayout _contenido;

        public StackLayout Contenido
        {
            get => _contenido;
            set => SetProperty(ref _contenido, value);
        }

        private string _titulo;

        public string Titulo
        {
            get => _titulo;
            set => SetProperty(ref _titulo, value);
        }

        #endregion

        #region Eventos
        public event EventHandler<List<string>> MandarAlerta;
        #endregion

        #region Variables Globales
        string[] arregloNombre;
        string nombrePais;
        GenericManager _genericManager;
        StackLayout _stackLayoutAuxiliar;
        #endregion

        #region Constructor
        public DetallePaisesViewModel(GenericManager genericManager, string pais)
        {
            if (pais.Contains(","))
            {
                arregloNombre = pais.Split(',');
                nombrePais = arregloNombre[0].Trim(' ');
            }
            else if(pais.Contains("("))
            {
                if (pais.Contains("Korea"))
                {
                    pais = pais.Replace("(", ",").Replace(")", ",");
                    arregloNombre = pais.Split(',');
                    nombrePais = $"{arregloNombre[1].Trim(' ')} {arregloNombre[0].Trim(' ')}";
                }
                else if (pais.Contains("Venezuela"))
                {
                    nombrePais = "Venezuela";
                }
                else if (pais.Contains("Vatican"))
                {
                    nombrePais = "Holy See";
                }
                else
                {
                    pais = pais.Replace("(", ",").Replace(")", ",");
                    arregloNombre = pais.Split(',');
                    nombrePais = $"{arregloNombre[1].Trim(' ')}";
                }
                pais = nombrePais;
            }
            else
            {
                nombrePais = pais;
            }
            _genericManager = genericManager;
            this.Titulo = pais;
        }

        public void Actualizar()
        {
            _stackLayoutAuxiliar = new StackLayout();
            if (_genericManager.TraerHistorialDeUnPais(nombrePais) is List<History> histories)
            {
                if (histories.Count > 0)
                {
                    PlotView plotView = new PlotView { WidthRequest = 100, HeightRequest = 200 };
                    PlotModel plotModel = new PlotModel { LegendOrientation = LegendOrientation.Vertical };
                    DateTimeAxis ejeTiempo = new DateTimeAxis { Angle = 90 };
                    LineSeries muertoSerie = new LineSeries { Title = $"Muertos: {histories.Last().Deaths}"};
                    LineSeries recuperadosSerie = new LineSeries { Title = $"Recuperados: {histories.Last().Recovered}" };
                    LineSeries confirmadosSerie = new LineSeries { Title = $"Confirmados: {histories.Last().Confirmed}" };
                    foreach (History dia in histories.OrderByDescending(history => history.Date).Take(20).OrderBy(history => history.Date))
                    {
                        muertoSerie.Points.Add(DateTimeAxis.CreateDataPoint(dia.Date.DateTime, dia.Deaths));
                        recuperadosSerie.Points.Add(DateTimeAxis.CreateDataPoint(dia.Date.DateTime, dia.Recovered));
                        confirmadosSerie.Points.Add(DateTimeAxis.CreateDataPoint(dia.Date.DateTime, dia.Confirmed));
                    }

                    plotModel.Axes.Add(ejeTiempo);
                    plotModel.Series.Add(muertoSerie);
                    plotModel.Series.Add(recuperadosSerie);
                    plotModel.Series.Add(confirmadosSerie);
                    plotView.Model = plotModel;
                    _stackLayoutAuxiliar.Children.Add(plotView);
                    this.Contenido = _stackLayoutAuxiliar;
                }


                //if (histories.Count > 0)
                //{
                //    ChartView chartView = new ChartView { WidthRequest = 100, HeightRequest = 200 };
                //    LineChart graficaLineas;
                //    List<Microcharts.Entry> entriesDeaths = new List<Microcharts.Entry>();
                //    foreach (History dia in histories.OrderByDescending(history => history.Date).Take(20).OrderBy(history => history.Date))
                //    {
                //        RGB color = new RGB((byte)new Random().Next(0, 256), (byte)new Random().Next(0, 256), (byte)new Random().Next(0, 256));
                //        string colorHex = Herramientas.RGBToHexadecimal(color);
                //        entriesDeaths.Add(new Microcharts.Entry(dia.Confirmed) { Color = SkiaSharp.SKColor.Parse(colorHex), Label = dia.Date.DateTime.ToShortDateString(), ValueLabel = dia.Confirmed.ToString("#,#") });
                //    }
                //    graficaLineas = new LineChart { Entries = entriesDeaths };
                //    chartView.Chart = graficaLineas;
                //    _stackLayoutAuxiliar.Children.Add(new Label { Text = "Casos Confirmados", FontAttributes = FontAttributes.Bold });
                //    _stackLayoutAuxiliar.Children.Add(chartView);
                //    this.Contenido = _stackLayoutAuxiliar;
                //}
                //else
                //{
                //    MandarAlerta(this, new List<string> { "COVID 19", "No se obtuvieron datos a mostrar", "OK" });
                //}
            }
            else
            {
                MandarAlerta(this, new List<string> { "COVID 19", "No se obtuvieron datos a mostrar", "OK" });
            }
        }
        #endregion
    }
}
