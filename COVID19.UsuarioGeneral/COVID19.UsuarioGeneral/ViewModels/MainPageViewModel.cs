﻿using COVID19.BIZ.API;
using COVID19.COMMON.Entidades;
using COVID19.UsuarioGeneral.Models;
using COVID19.UsuarioGeneral.Tools;
using COVID19.UsuarioGeneral.Views;
using Microcharts;
using Microcharts.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace COVID19.UsuarioGeneral.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        #region Propiedad
        private StackLayout _contenido;

        public StackLayout Contenido
        {
            get => _contenido;
            set => SetProperty(ref _contenido, value);
        }
        #endregion

        #region Variables Globales
        StackLayout _stackLayoutAuxiliar;
        GenericManager _genericManager;
        #endregion

        #region Constructor
        public MainPageViewModel()
        {
            _stackLayoutAuxiliar = new StackLayout();
            LlenarDatos();
        }

        private void LlenarDatos()
        {
            _genericManager = new GenericManager();
            LlenarGrafica();
        }

        private void LlenarGrafica()
        { 
            Summary summary = _genericManager.ListarPaises();
            string nombrePais = "";
            string[] arregloNombre;
            foreach (Country pais in summary.Countries.OrderByDescending(country => country.TotalConfirmed).ThenBy(country => country.TotalDeaths))
            {
                if (pais.TotalConfirmed == 0 || pais.TotalDeaths == 0)
                {
                    _stackLayoutAuxiliar.Children.Add(new PaisesView(_genericManager, new TarjetaModel { NumeroConfirmados = pais.TotalConfirmed.ToString(), NumeroMuerte = pais.TotalDeaths.ToString(), Titulo = pais.CountryCountry }));
                }
                else
                {
                    _stackLayoutAuxiliar.Children.Add(new PaisesView(_genericManager, new TarjetaModel { NumeroConfirmados = pais.TotalConfirmed.ToString("#,#"), NumeroMuerte = pais.TotalDeaths.ToString("#,#"), Titulo = pais.CountryCountry }));
                }
            }
            this.Contenido = _stackLayoutAuxiliar;
        }
        #endregion
    }
}