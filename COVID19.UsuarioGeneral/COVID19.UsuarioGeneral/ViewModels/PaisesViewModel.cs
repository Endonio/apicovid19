﻿using COVID19.BIZ.API;
using COVID19.UsuarioGeneral.Models;
using COVID19.UsuarioGeneral.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace COVID19.UsuarioGeneral.ViewModels
{
    public class PaisesViewModel:BaseViewModel
    {
        #region Propiedades
        private TarjetaModel _tarjeta;

        public TarjetaModel Tarjeta
        {
            get => _tarjeta;
            set => SetProperty(ref _tarjeta, value);
        }
        #endregion

        #region Comandos
        public ICommand SeleccionarTarjetaCommand { get; set; }
        #endregion

        #region Eventos
        public event EventHandler<Page> CambiarPagina;
        #endregion

        #region Variables Globales
        GenericManager _genericManager;
        string _nombrePais;
        #endregion

        #region Constructor
        public PaisesViewModel(GenericManager genericManager,TarjetaModel informacionTarjeta)
        {
            this.Tarjeta = informacionTarjeta;
            this.SeleccionarTarjetaCommand = new Command(CambiarPaginaExec);
            _genericManager = genericManager;
            _nombrePais = informacionTarjeta.Titulo;
        }
        #endregion

        #region Métodos
        private void CambiarPaginaExec() => CambiarPagina(this, new DetallePaisesView(_genericManager, _nombrePais));
        #endregion
    }
}
