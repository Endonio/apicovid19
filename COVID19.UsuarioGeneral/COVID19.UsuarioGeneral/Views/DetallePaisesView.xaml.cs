﻿using COVID19.BIZ.API;
using COVID19.UsuarioGeneral.Tools;
using COVID19.UsuarioGeneral.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COVID19.UsuarioGeneral.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallePaisesView : ContentPage
    {
        public DetallePaisesView(GenericManager genericManager,string pais)
        {
            InitializeComponent();
            this.BindingContext = new DetallePaisesViewModel(genericManager, pais);
            (this.BindingContext as DetallePaisesViewModel).MandarAlerta += DetallePaisesView_MandarAlerta;
        }

        private void DetallePaisesView_MandarAlerta(object sender, System.Collections.Generic.List<string> e) => DisplayAlert(e[0], e[1], e[2]);

        protected override void OnAppearing()
        {
            base.OnAppearing();
            DependencyService.Get<IOrientacion>().CambiarOrientacion(1);
            (this.BindingContext as DetallePaisesViewModel).Actualizar();
        }
    }
}