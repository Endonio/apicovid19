﻿using COVID19.BIZ.API;
using COVID19.COMMON.Entidades;
using COVID19.UsuarioGeneral.Tools;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;

namespace COVID19.UsuarioGeneral
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            //GenericManager genericManager = new GenericManager();
            //Summary paises = genericManager.ListarPaises();
            //pckPaises.ItemsSource = paises.Countries.Select(pais => pais.CountryCountry).ToList();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            DependencyService.Get<IOrientacion>().CambiarOrientacion(0);
        }
    }
}
