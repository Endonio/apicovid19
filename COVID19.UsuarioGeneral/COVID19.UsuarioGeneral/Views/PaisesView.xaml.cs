﻿using COVID19.BIZ.API;
using COVID19.UsuarioGeneral.Models;
using COVID19.UsuarioGeneral.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COVID19.UsuarioGeneral.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaisesView : ContentView
    {
        public PaisesView(GenericManager genericManager, TarjetaModel informacionTarjeta)
        {
            InitializeComponent();
            this.BindingContext = new PaisesViewModel(genericManager, informacionTarjeta);
            (this.BindingContext as PaisesViewModel).CambiarPagina += PaisesView_CambiarPagina; ;
        }

        private void PaisesView_CambiarPagina(object sender, Page e) => this.Navigation.PushAsync(e);
    }
}